package com.mcmxc;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.*;

import java.io.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Main {

    private static XSSFCellStyle createStyleForTitle(XSSFWorkbook workbook) {
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        XSSFCellStyle style = workbook.createCellStyle();
        style.setFont(font);
        return style;
    }

    public static void main(String[] args) throws IOException {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Result");

        List<ResultOfTest> list = ResultDao.listResult();

        sheet.autoSizeColumn(15);
        sheet.setColumnWidth(11, 5000);
        sheet.setDefaultRowHeight((short) 2500);

        int rownum = 0;
        Cell cell;
        Row row;

        XSSFCellStyle style = createStyleForTitle(workbook);

        row = sheet.createRow(rownum);

        cell = row.createCell(0, CellType.NUMERIC);
        cell.setCellValue("ID");
        cell.setCellStyle(style);


        cell = row.createCell(1, CellType.NUMERIC);
        cell.setCellValue("Time in sec");
        cell.setCellStyle(style);


        cell = row.createCell(2, CellType.NUMERIC);
        cell.setCellValue("Style Scale");
        cell.setCellStyle(style);

        cell = row.createCell(3, CellType.NUMERIC);
        cell.setCellValue("Original Colors");
        cell.setCellStyle(style);


        cell = row.createCell(4, CellType.NUMERIC);
        cell.setCellValue("Content weight");
        cell.setCellStyle(style);


        cell = row.createCell(5, CellType.NUMERIC);
        cell.setCellValue("Style Weight");
        cell.setCellStyle(style);


        cell = row.createCell(6, CellType.NUMERIC);
        cell.setCellValue("Iterations");
        cell.setCellStyle(style);

        cell = row.createCell(7, CellType.STRING);
        cell.setCellValue("Optimizer");
        cell.setCellStyle(style);

        cell = row.createCell(8, CellType.NUMERIC);
        cell.setCellValue("Learning rate");
        cell.setCellStyle(style);

        cell = row.createCell(9, CellType.NUMERIC);
        cell.setCellValue("Image size");
        cell.setCellStyle(style);

        cell = row.createCell(10, CellType.STRING);
        cell.setCellValue("Command");
        cell.setCellStyle(style);

        cell = row.createCell(11, CellType.STRING);
        cell.setCellValue("Image");
        cell.setCellStyle(style);

        File file = new File("/home/notroot/result-of-test.xlsx");
        //File file = new File("C:\\input\\result-of-test.xlsx");

        try {
            FileOutputStream outFile = new FileOutputStream(file);
            workbook.write(outFile);
        } catch (IOException e) {
            System.out.println("exception in writing excel file");
        }
        System.out.println("Created file: " + file.getAbsolutePath());


        for (ResultOfTest resultOfTest : list) {
            rownum++;


            row = sheet.createRow(rownum);

            cell = row.createCell(0, CellType.NUMERIC);
            cell.setCellValue(resultOfTest.getId());

            cell = row.createCell(2, CellType.NUMERIC);
            cell.setCellValue(resultOfTest.getStyleScale());

            cell = row.createCell(3, CellType.NUMERIC);
            cell.setCellValue(resultOfTest.getOriginalColors());

            cell = row.createCell(4, CellType.NUMERIC);
            cell.setCellValue(resultOfTest.getContentWeight());

            cell = row.createCell(5, CellType.NUMERIC);
            cell.setCellValue(resultOfTest.getStyleWeight());

            cell = row.createCell(6, CellType.NUMERIC);
            cell.setCellValue(resultOfTest.getIterations());

            cell = row.createCell(7, CellType.STRING);
            cell.setCellValue(resultOfTest.getOptimizer());

            cell = row.createCell(8, CellType.NUMERIC);
            cell.setCellValue(resultOfTest.getLearningRate());

            cell = row.createCell(9, CellType.NUMERIC);
            cell.setCellValue(resultOfTest.getImageSize());

            cell = row.createCell(10, CellType.STRING);
            cell.setCellValue(resultOfTest.getCurrentShellCommand().toString());

            long start = System.nanoTime();
            //Process process = Runtime.getRuntime().exec(new String[]{"cmd.exe", "/c", "ping -n 3 google.com"});
            Process process = Runtime.getRuntime().exec(new String[]{"bash", "-c", resultOfTest.getCurrentShellCommand().toString()});
            try {
                process.waitFor(300, TimeUnit.SECONDS);
                process.destroy();
            } catch (InterruptedException e) {
                System.out.println("Interrupted exception");
            }
            long elapsedTime = System.nanoTime() - start;
            long convert = TimeUnit.SECONDS.convert(elapsedTime, TimeUnit.NANOSECONDS);

            cell = row.createCell(1, CellType.NUMERIC);
            cell.setCellValue(convert);

            File resultFile = new File(resultOfTest.getResultFile());

            if (resultFile.exists()) {
                InputStream my_banner_image = new FileInputStream(resultFile);
                byte[] bytes = IOUtils.toByteArray(my_banner_image);
                int my_picture_id = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
                my_banner_image.close();
                /* Create the drawing container */
                XSSFDrawing drawing = sheet.createDrawingPatriarch();
                /* Create an anchor point */
                XSSFClientAnchor my_anchor = new XSSFClientAnchor();
                /* Define top left corner, and we can resize picture suitable from there */
                my_anchor.setCol1(11);
                my_anchor.setRow1(rownum);
                //my_anchor.setCol2(3);
                //my_anchor.setRow2(2);
                /* Invoke createPicture and pass the anchor point and ID */
                XSSFPicture my_picture = drawing.createPicture(my_anchor, my_picture_id);
                /* Call resize method, which resizes the image */
                my_picture.resize(1, 1);


                // Write File
                FileOutputStream out = new FileOutputStream(file);
                workbook.write(out);
                out.close();
            } else {
                cell = row.createCell(11, CellType.STRING);
                cell.setCellValue("More than 5 min");
            }


        }
        System.out.println("ready");

    }
}
