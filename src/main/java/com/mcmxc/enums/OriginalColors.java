package com.mcmxc.enums;

public enum OriginalColors {
    FIRST(1),
    SECOND(0);

    private final int value;

    private OriginalColors(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
