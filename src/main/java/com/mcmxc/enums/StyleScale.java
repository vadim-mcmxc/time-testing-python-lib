package com.mcmxc.enums;

public enum  StyleScale {
    /*FIRST(4f),
    SECOND(2f),*/
    THIRD(1f)/*,
    FOURTH(.5f)*/;

    private final float value;

    private StyleScale(float value) {
        this.value = value;
    }

    public float getValue() {
        return value;
    }

}
