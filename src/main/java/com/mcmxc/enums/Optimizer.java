package com.mcmxc.enums;

public enum  Optimizer {
    ADAM("adam")/*,
    LBFGS("lbfgs")*/;

    private final String value;

    private Optimizer(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
