package com.mcmxc.enums;

public enum LearningRate {
   /* FIRST(5),*/
    SECOND(10),
    /*THIRD(20)*/;

    private final Integer value;

    private LearningRate(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

}
