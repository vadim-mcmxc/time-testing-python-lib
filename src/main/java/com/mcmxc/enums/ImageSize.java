package com.mcmxc.enums;

public enum ImageSize {
    FIRST(512),
    SECOND(1024);

    private final int value;

    private ImageSize(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
