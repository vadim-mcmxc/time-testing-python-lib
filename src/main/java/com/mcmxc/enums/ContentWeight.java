package com.mcmxc.enums;

public enum ContentWeight {
    /*FIRST(1),
    SECOND(5),*/
    THIRD(10)/*,
    FOURTH(50)*/;

    private final int value;

    private ContentWeight(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
