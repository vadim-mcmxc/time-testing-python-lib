package com.mcmxc.enums;

public enum Iterations {
    /*FIRST(200),*/
    SECOND(500)/*,
    THIRD(1000),
    FOURTH(200)*/;

    private final int value;

    private Iterations(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
