package com.mcmxc.enums;

public enum StyleWeight {
   /* FIRST(10),
    SECOND(100),*/
    THIRD(200);

    private final int value;

    private StyleWeight(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
