package com.mcmxc;

import com.mcmxc.enums.*;
import com.mcmxc.enums.OriginalColors;

import java.util.ArrayList;
import java.util.List;

public class ResultDao {
    public static List<ResultOfTest> listResult() {
        List<ResultOfTest> list = new ArrayList<ResultOfTest>();
        long count = 0;



        for (StyleScale styleScale : StyleScale.values()) {

            for (OriginalColors originalColors : OriginalColors.values()) {

                for (ContentWeight contentWeight : ContentWeight.values()) {

                    for (StyleWeight styleWeight : StyleWeight.values()) {

                        for (Iterations iterations : Iterations.values()) {

                            for (Optimizer optimizer : Optimizer.values()) {
                                if (optimizer == Optimizer.ADAM) {

                                    for (LearningRate learningRate : LearningRate.values()) {

                                        for (ImageSize imageSize : ImageSize.values()) {
                                            StringBuilder shellCommand = new StringBuilder("cd /opt/neural_style && python3 neural_style.py" +
                                                    " -style_image /home/notroot/neural-style-pt/inputs/test_style_image.jpg" +
                                                    " -content_image /home/notroot/neural-style-pt/inputs/test_content_image.png");
                                            shellCommand.append(" -style_scale " + styleScale.getValue());
                                            shellCommand.append(" -original_colors " + originalColors.getValue());
                                            shellCommand.append(" -content_weight " + contentWeight.getValue());
                                            shellCommand.append(" -style_weight " + styleWeight.getValue());
                                            shellCommand.append(" -num_iterations " + iterations.getValue() + " -cudnn_autotune");
                                            shellCommand.append(" -seed 123 -content_layers relu0,relu3,relu7,relu12 -style_layers relu0,relu3,relu7,relu12");
                                            shellCommand.append(" -optimizer adam");
                                            shellCommand.append(" -learning_rate " + learningRate.getValue());

                                            shellCommand.append(" -image_size " + imageSize.getValue());

                                            shellCommand.append(" -output_image /home/notroot/neural-style-pt/outputs/" +
                                                    "result_" + count + ".png" + " -model_file models/nin_imagenet.pth -gpu 0 -backend cudnn");
                                            ResultOfTest resultOfTest = new ResultOfTest(shellCommand, count, 0, styleScale.getValue(),
                                                    originalColors.getValue(), contentWeight.getValue(), styleWeight.getValue(),
                                                    iterations.getValue(), optimizer.getValue(), learningRate.getValue(), imageSize.getValue(),
                                                    //"/home/notroot/neural-style-pt/outputs/" + "result_" + count + ".png");
                                                    "C:\\input\\" + "result_" + count + ".png");
                                            count++;
                                            list.add(resultOfTest);
                                        }

                                    }

                                } else {
                                    for (ImageSize imageSize : ImageSize.values()) {
                                        StringBuilder shellCommand = new StringBuilder("cd /opt/neural_style && python3 neural_style.py" +
                                                " -style_image /home/notroot/neural-style-pt/inputs/test_style_image.jpg" +
                                                " -content_image /home/notroot/neural-style-pt/inputs/test_content_image.png");
                                        shellCommand.append(" -style_scale " + styleScale.getValue());
                                        shellCommand.append(" -original_colors " + originalColors.getValue());
                                        shellCommand.append(" -content_weight " + contentWeight.getValue());
                                        shellCommand.append(" -style_weight " + styleWeight.getValue());
                                        shellCommand.append(" -num_iterations " + iterations.getValue() + " -cudnn_autotune");
                                        shellCommand.append(" -seed 123 -content_layers relu0,relu3,relu7,relu12 -style_layers relu0,relu3,relu7,relu12");
                                        shellCommand.append(" -optimizer lbfgs");
                                        shellCommand.append(" -image_size " + imageSize.getValue());

                                        shellCommand.append(" -output_image /home/notroot/neural-style-pt/outputs/" +
                                                "result_" + count + ".png" + " -model_file models/nin_imagenet.pth -gpu 0 -backend cudnn");
                                        ResultOfTest resultOfTest = new ResultOfTest(shellCommand, count, 0, styleScale.getValue(),
                                                originalColors.getValue(), contentWeight.getValue(), styleWeight.getValue(),
                                                iterations.getValue(), optimizer.getValue(), imageSize.getValue(),
                                                //"/home/notroot/neural-style-pt/outputs/" + "result_" + count + ".png");
                                                "C:\\input\\" + "result_" + count + ".png");
                                        count++;
                                        list.add(resultOfTest);
                                    }

                                }
                            }

                        }

                    }

                }

            }

        }


        return list;
    }
}
