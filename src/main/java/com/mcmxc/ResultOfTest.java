package com.mcmxc;


import lombok.Data;

@Data
public class ResultOfTest {


    private StringBuilder currentShellCommand;
    private long id;
    private long timeOfTestInSeconds;
    private float styleScale;
    private int originalColors;
    private int contentWeight;
    private int styleWeight;
    private int iterations;
    private String optimizer;
    private int learningRate;
    private int imageSize;
    private String resultFile;

    public ResultOfTest(StringBuilder currentShellCommand, long id, long timeOfTestInSeconds, float styleScale, int originalColors, int contentWeight, int styleWeight, int iterations, String optimizer, int learningRate, int imageSize, String resultFile) {
        this.currentShellCommand = currentShellCommand;
        this.id = id;
        this.timeOfTestInSeconds = timeOfTestInSeconds;
        this.styleScale = styleScale;
        this.originalColors = originalColors;
        this.contentWeight = contentWeight;
        this.styleWeight = styleWeight;
        this.iterations = iterations;
        this.optimizer = optimizer;
        this.learningRate = learningRate;
        this.imageSize = imageSize;
        this.resultFile = resultFile;
    }

    public ResultOfTest(StringBuilder currentShellCommand, long id, long timeOfTestInSeconds, float styleScale, int originalColors, int contentWeight, int styleWeight, int iterations, String optimizer, int imageSize, String resultFile) {
        this.currentShellCommand = currentShellCommand;
        this.id = id;
        this.timeOfTestInSeconds = timeOfTestInSeconds;
        this.styleScale = styleScale;
        this.originalColors = originalColors;
        this.contentWeight = contentWeight;
        this.styleWeight = styleWeight;
        this.iterations = iterations;
        this.optimizer = optimizer;
        this.imageSize = imageSize;
        this.resultFile = resultFile;
    }
}
